// fetch()
//     .then(response => response.json())
//     .then(val => console.log(val))

/**
  * Una funzione asincrona inizia la sua firma con la chiave async
  * All'interno delle funzioni asincrone posso utilizzare la prola chiave "await"
*/

async function myAsyncFunc() {
    // Await serve a mettersi in ascolto ad una promise
    const response = await fetch();
    const val = await response.json();

    // const p = new Promise((resolve, reject) => {
    //     return resolve('info');
    // });

    // const successo = await p;
}