// const xhr = XMLHttpRequest();
// xhr.open('GET', 'https://');

// xhr.addEventListener('loadend', () => {
//     console.log('data receive'); //JSON da decodificare con JSON.parse();
//     xhr.resonse;
// });

// xhr.send();

/**
 * Fetch fa parte delle API di ES6
 * A differenza di XHR, che lavora per eventi, è basato sulle promise
 * Di default il verbo usato per l'interrogazione è il GET
*/

fetch('http://jsonplaceholder.typicode.com/users').then(
    // JSON.parse(res.body); gestisce la maggior parte dei casi, compresi errori (404, 500 etc etc...).
    // res.json() restituisce una Promise che risolverà con il contenuto della risposta
    // ottenuta con la richiesta
    res => {
        if(res.status !== 200) {
            console.error('qualcosa è andato male!');
            return;
        }
        return res.json();
    },
    
    // Non restituisce casi particolari di errore se non quelli
    // strettamente legati ad una richiesta che non è partita al backend
    err => console.error(err) 

).then (
    data => console.log(data)
)

// const p = new Promise((resolve, reject) => {
//     return resolve(100);
// });

// p.then(
//     res => console.log(res),
//     err => console.error(err)
// )

//      esempio richiesta POST

fetch('http://jsonplaceholder.typicode.com/users', {
    // Il secondo argomentopermette di dettagliare, tra le altre cose,
    // il body della richiesta per quei verbi HTTP che ne fanno uso, gli header etc...
    // il corpo della richiesta, contenente le informazioni necessarie al backend per creare,
    // in questo caso, un nuovo post. Il body deve essere in formato JSON
    body: JSON.stringify({
        body: 'Corpo del post',
        title: 'Titolo del post',
        userId: 1
    }),
    // Gli headers dettagliano la truttura della richiesta. Qui viene eventualmente
    // aggiunto il token di autenticazione
    headers: {
        'Content-Type': 'application/json; charset=UTF-8'
    },
    method: 'POST'
}).then(res => res.json())
  .then(data => console.log(data));

