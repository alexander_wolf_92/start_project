// javascript

/**
 * Riscrivere l'eserizio della tabella inserendo gli utenti non più con la concatenazione di innerText,
 * bensì aggiungendo elementi HTML creati con document.createElement
 */

/**
 *
 * @param {number} id
 * @param {string} email
 * @param {string} name
 */

// esportare solo il costruttore inserendo la parola export prima della funzione“

export function User(id, email, name) {
  this.email = email;
  this.id = id;
  this.name = name;
}
