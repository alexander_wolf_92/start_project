import { login } from "./function/users";

const email = document.querySelector('input[name="email"]');
const password = document.querySelector('input[name="password"]');
const form = document.querySelector('form');

form.addEventListener('submit', e => {
    e.preventDefault();
    login(email.value, password.value).then(data => console.log(data));
});

