fetch('http://jsonplaceholder.typicode.com/users', {
    // Il secondo argomento permette di dettagliare, tra le altre cose,
    // il body della richiesta per quei verbi HTTP che ne fanno uso, gli header etc...
    // il corpo della richiesta, contenente le informazioni necessarie al backend per creare,
    // in questo caso, un nuovo post. Il body deve essere in formato JSON
    body: JSON.stringify({
        body: 'Corpo del post',
        title: 'Titolo del post',
        userId: 1
    }),
    // Gli headers dettagliano la truttura della richiesta. Qui viene eventualmente
    // aggiunto il token di autenticazione
    headers: {
        'Content-Type': 'application/json; charset=UTF-8'
    },
    method: 'POST'
}).then(res => res.json())
  .then(data => console.log(data));