//javascript

import { User } from "../models/users";

export function getUsers(cb) {
  var xhr = new XMLHttpRequest();
  // prende due informazione => method('')
  xhr.open("GET", "https://jsonplaceholder.typicode.com/users");
  //funzione da eseguire quando riceviamo i dati
  // json una serie di regole che stabiliscono come rappresentare attraverso una stringa aray ed oggetti con le loro chiavi e valori.
  xhr.addEventListener("loadend", function () {
    //prende una stringa e la trasforma in un array
    //
    var data = JSON.parse(xhr.response);
    var users = data.map(function (u) {
      return new User(u.id, u.email, u.name);
    });
    cb(users);
  });
  // risposta alla chiamata
  xhr.send();
}
