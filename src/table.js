
// importo costruttore User dal file users.js dentro models‘
import { User } from "./models/users";
// importo il costruttore getUsers dalla cartella functions file users.js
import { getUsers } from "./functions/users";

// richiamo la funzione getUsers con chiamata esterna a utenti

var tbody = document.getElementById("tbody");

getUsers(function (users) {
  users.forEach(function (u) {
    tb.innerHTML +=
      '<tr id="' +
      u.id +
      '"><td>' +
      u.id +
      "</td><td>" +
      u.email +
      "</td><td>" +
      u.name +
      "</td><td><button>Delete</button></tr>";
  });

  var buttons = document.querySelectorAll("button");
  buttons.forEach(function (b) {
    b.addEventListener("click", function (e) {
      // Riferimento alla riga sulla quale l'utente ha fatto click (click sul bottone "delete")
      var r = e.target.parentElement.parentElement;
      // Su ciascuna riga, abbiamo inserito nell'attributo "id"
      // il valore "id" dell'utente
      // Questo serve a poter ricercare l'utente da elminare all'interno dell'array
      // Itero su ciascun elemento dell'array fintanto che non individuo l'utente che ha
      // un id uguale alla variabile userId
      var userId = r.id;

      r.remove();

      var pos = users.findIndex(function (u) {
        return u.id == userId;
      });

      // Parametro 1: elimina a partire dalla posizione indicata
      // Parametro 2: numero di elementi da eliminare
      users.splice(pos, 1);
      console.log(users);
    });
  });
});

// vecchio esercizio costruttore
var users = [
  new User(1, "utente_uno.com", "Uno"),
  new User(2, "utente_due.com", "Due"),
  new User(3, "utente_tre.com", "Tre"),
];
