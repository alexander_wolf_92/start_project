// Restituendo la Promise che trasforma il JSON in una struttura dati
// quale array od oggetto, posso invocare il metodo login in qualsiasi
// punto de mio script ed agganciarmi alla risposta con il metodo then
// src/users.js
export const login = (email, password) => {
    return fetch('https://reqres.in/api/login', {
        body: JSON.stringify({
            email,
            password
        }),
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }, 
        method: 'POST'
    }).then(res => res.json())
//.then(data => cb(data));
}