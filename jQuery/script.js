/**
 *  Add tag <span> and class .plus with css rules
 *  <li> add class .heart and css rule   
*/
const $add = $(function() {
    $('li.color').prepend('<span>' + '+' + '</span>');
    var $plus = $('li span').addClass('plus');
    $plus.css({
        'color': 'yellow',
        'margin-right': '10px'
    });
    $('li.color').addClass('heart');
    $('li.color').css({
        'cursor': 'pointer'
    });
});

/**
 * function on click remove tag <li>
*/

// var $remove = $('li.color').on('click', function(){
//     $(this).remove();
// });

/**
 * try to change animate with:
 * .toggle
 * .slideToggle
 * .slideUp
 * .hide
*/
const $remove = $('li:lt(3)').on('click', function() {
    $(this).animate({
        opacity: 0.0,
        paddingLeft: '+=80'
    }, 500, function(){
        $(this).remove();
    });
});

/* when you open the browser start this animation */
const $start_animation = $(function() {
    $('li:first').hide().delay(500).fadeIn(1400);
    $('li:nth-child(2)').hide().delay(600).fadeIn(1500);
    $('li:nth-child(3)').hide().delay(700).fadeIn(1500);
    $('li:nth-child(4)').hide().delay(800).fadeIn(1500);
});